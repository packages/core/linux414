# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard@manjaro.org>

# Arch credits:
# Tobias Powalowski <tpowa@archlinux.org>
# Thomas Baechler <thomas@archlinux.org>

pkgbase=linux414
pkgname=('linux414' 'linux414-headers')
_kernelname=-MANJARO
_basekernel=4.14
_basever=414
_aufs=20190902 # last version
_bfq=v8r12
_bfqdate=20171108
_bfqdate2=20180404
pkgver=4.14.280
pkgrel=1
arch=('x86_64')
url="https://www.kernel.org/"
license=('GPL2')
makedepends=('bc'
    'docbook-xsl'
    'libelf'
    'pahole'
    'git'
    'inetutils'
    'kmod'
    'xmlto'
    'cpio'
    'perl'
    'tar'
    'xz')
options=('!strip')
source=("https://www.kernel.org/pub/linux/kernel/v4.x/linux-${_basekernel}.tar.xz"
        "https://www.kernel.org/pub/linux/kernel/v4.x/patch-${pkgver}.xz"
        # the main kernel config files
        'config'
        # AUFS Patches
        "aufs4.14.73+-${_aufs}.patch"
        'aufs4-base.patch'
        'aufs4-kbuild.patch'
        'aufs4-loopback.patch'
        'aufs4-mmap.patch'
        'aufs4-standalone.patch'
        'tmpfs-idr.patch'
        'vfs-ino.patch'
        1301-BFQ-${_bfq}-${_bfqdate}.patch
        1302-BFQ-${_bfq}-${_bfqdate2}.patch
        # ARCH Patches
        '0001-add-sysctl-to-disallow-unprivileged-CLONE_NEWUSER-by.patch'
        # MANJARO Patches
        '0601-drm-i915-edp-Only-use-the-alternate-fixed-mode-if-it.patch'
        #"prepatch-${_basekernel}.patch"
        # Zen temperature
        '1101-zen-temp.patch'
        '1102-zen-temp.patch'
        '1103-zen-temp.patch'
        # Bootsplash
        '0401-revert-fbcon-remove-now-unusued-softback_lines-cursor-argument.patch'
        '0402-revert-fbcon-remove-soft-scrollback-code.patch'
        '0501-bootsplash.patch'
        '0502-bootsplash.patch'
        '0503-bootsplash.patch'
        '0504-bootsplash.patch'
        '0505-bootsplash.patch'
        '0506-bootsplash.patch'
        '0507-bootsplash.patch'
        '0508-bootsplash.patch'
        '0509-bootsplash.patch'
        '0510-bootsplash.patch'
        '0511-bootsplash.patch'
        '0512-bootsplash.patch'
        '0513-bootsplash.patch'
)
sha256sums=('f81d59477e90a130857ce18dc02f4fbe5725854911db1e7ba770c7cd350f96a7'
            'f2ebff5d0cd5300a5da9fbba5a73473e8a81a0264bf81435ebb212436cd117e1'
            'b98465aa74743756b25f019657d3a747d5aa350fe7420784193c4ca53b6df9bf'
            'b2edb99103b054867ad7512e6b9fcb607aeda9888b357884386397d00980bc71'
            'c0cb00f9cffa6eb195431d728c6f58c4b331e676c4edb52a965f64481277f99b'
            '3bfefe82b086d790a4f376356a9cddbec7a6aa998fab07bb46e708d2f80fb57a'
            '55921e1a3f27e042d9e6e8e2b795ca2cc227a310a12f7829e8ccd543fc1774c0'
            'f4238f5444258cbe88daa2712a57c18f394f36efd58536ccfe62865fd4998433'
            'ae9f609df74c09b42705f16dafd767121d595ba1c961051d30790912e6b19887'
            '26b49e4f39562f11d5412c80e595850c37184c6ee689462f6a766d6ad29c107e'
            '9b1f9fb9231af1d29214daa428a28eba3036d7543b5f7387d59fd6ec188c92f5'
            '5ac0d9fb774ed038c5537c59836b389bb64bdb50a01f32d0ee8ae03159ca9198'
            'f51c1b8709bf9f0e642ebd12d48ee1ac41047902a87cf6a783ca794cfa5142dc'
            '37b86ca3de148a34258e3176dbf41488d9dbd19e93adbd22a062b3c41332ce85'
            'c08d12c699398ef88b764be1837b9ee11f2efd3188bd1bf4e8f85dfbeee58148'
            'ee46e4c25b58d1dbd7db963382cf37aeae83dd0b4c13a59bdd11153dc324d8e8'
            'cd463af7193bcf864c42e95d804976a627ac11132886f25e04dfc2471c28bf6c'
            '70cee696fb4204ac7f787cef0742c50637e8bb7f68e2c7bca01aeefff32affc8'
            '6381c2b4c9c93c742f2d165d2552f5fa80524b388c6ee7ba12a01965f6572725'
            '77da1185db1f499b4749e0e9f3f613e8d5835bf372937c0e5edf76451ae67259'
            'a504f6cf84094e08eaa3cc5b28440261797bf4f06f04993ee46a20628ff2b53c'
            'e096b127a5208f56d368d2cb938933454d7200d70c86b763aa22c38e0ddb8717'
            '8c1c880f2caa9c7ae43281a35410203887ea8eae750fe8d360d0c8bf80fcc6e0'
            '1144d51e5eb980fceeec16004f3645ed04a60fac9e0c7cf88a15c5c1e7a4b89e'
            'dd4b69def2efacf4a6c442202ad5cb93d492c03886d7c61de87696e5a83e2846'
            'c8f9cd8ffdbc49d1d02852409b2532247f1deef2472566c1d16b52af744532cd'
            'c8b0cb231659d33c3cfaed4b1f8d7c8305ab170bdd4c77fce85270d7b6a68000'
            '8dbb5ab3cb99e48d97d4e2f2e3df5d0de66f3721b4f7fd94a708089f53245c77'
            'a7aefeacf22c600fafd9e040a985a913643095db7272c296b77a0a651c6a140a'
            'e9f22cbb542591087d2d66dc6dc912b1434330ba3cd13d2df741d869a2c31e89'
            '27471eee564ca3149dd271b0817719b5565a9594dc4d884fe3dc51a5f03832bc'
            '60e295601e4fb33d9bf65f198c54c7eb07c0d1e91e2ad1e0dd6cd6e142cb266d'
            '035ea4b2a7621054f4560471f45336b981538a40172d8f17285910d4e0e0b3ef')
prepare() {
  cd "linux-${_basekernel}"

  # add upstream patch
  msg "add upstream patch"
  patch -p1 -i "../patch-${pkgver}"
  chmod +x tools/objtool/sync-check.sh # GNU patch doesn't support git-style file mode

  msg "drm i915 patch"
  # https://bugs.freedesktop.org/show_bug.cgi?id=103497
  patch -Np1 -i "../0601-drm-i915-edp-Only-use-the-alternate-fixed-mode-if-it.patch"

  msg "disable USER_NS for non-root users"
  patch -Np1 -i "../0001-add-sysctl-to-disallow-unprivileged-CLONE_NEWUSER-by.patch"

  msg "add support for temperature sensors on Family 17h (Ryzen) processors"
  patch -Np1 -i "../1101-zen-temp.patch"
  patch -Np1 -i "../1102-zen-temp.patch"
  patch -Np1 -i "../1103-zen-temp.patch"

  msg "add bootsplash"
  msg2 "401"
  patch -Np1 -i "../0401-revert-fbcon-remove-now-unusued-softback_lines-cursor-argument.patch"
  msg2 "402"
  patch -Np1 -i "../0402-revert-fbcon-remove-soft-scrollback-code.patch"
  msg2 "501"
  patch -Np1 -i "../0501-bootsplash.patch"
  patch -Np1 -i "../0502-bootsplash.patch"
  patch -Np1 -i "../0503-bootsplash.patch"
  patch -Np1 -i "../0504-bootsplash.patch"
  patch -Np1 -i "../0505-bootsplash.patch"
  patch -Np1 -i "../0506-bootsplash.patch"
  patch -Np1 -i "../0507-bootsplash.patch"
  patch -Np1 -i "../0508-bootsplash.patch"
  patch -Np1 -i "../0509-bootsplash.patch"
  patch -Np1 -i "../0510-bootsplash.patch"
  patch -Np1 -i "../0511-bootsplash.patch"
  patch -Np1 -i "../0512-bootsplash.patch"
  # use git-apply to add binary files
  git apply -p1 < "../0513-bootsplash.patch"

  msg "add aufs4 support"
  patch -Np1 -i "../aufs4.14.73+-${_aufs}.patch"
  patch -Np1 -i "../aufs4-base.patch"
  patch -Np1 -i "../aufs4-kbuild.patch"
  patch -Np1 -i "../aufs4-loopback.patch"
  patch -Np1 -i "../aufs4-mmap.patch"
  patch -Np1 -i "../aufs4-standalone.patch"
  patch -Np1 -i "../tmpfs-idr.patch"
  patch -Np1 -i "../vfs-ino.patch"

  msg "add BFQ scheduler"
  msg "Fix naming schema in BFQ-MQ patch"
  sed -i -e "s/SUBLEVEL = 0/SUBLEVEL = $(echo ${pkgver} | cut -d. -f3)/g" \
      -i -e "s|EXTRAVERSION = -rc8|EXTRAVERSION =|1" \
      -i -e "s|NAME = Fearless Coyote|NAME = Petit Gorille|g" \
      "../1301-BFQ-${_bfq}-${_bfqdate}.patch"
  patch -Np1 -i "../1301-BFQ-${_bfq}-${_bfqdate}.patch"
  patch -Np1 -i "../1302-BFQ-${_bfq}-${_bfqdate2}.patch"

  cat "../config" > ./.config

  if [ "${_kernelname}" != "" ]; then
    sed -i "s|CONFIG_LOCALVERSION=.*|CONFIG_LOCALVERSION=\"${_kernelname}\"|g" ./.config
    sed -i "s|CONFIG_LOCALVERSION_AUTO=.*|CONFIG_LOCALVERSION_AUTO=n|" ./.config
  fi

  msg "set extraversion to pkgrel"
  sed -ri "s|^(EXTRAVERSION =).*|\1 -${pkgrel}|" Makefile

  msg "don't run depmod on 'make install'"
  # We'll do this ourselves in packaging
  sed -i '2iexit 0' scripts/depmod.sh

  msg "get kernel version"
  make prepare

  msg "rewrite configuration"
  yes "" | make config >/dev/null
}

build() {
  cd "linux-${_basekernel}"

  msg "build"
  make ${MAKEFLAGS} LOCALVERSION= bzImage modules
}

package_linux414() {
  pkgdesc="The ${pkgbase/linux/Linux} kernel and modules"
  depends=('coreutils' 'linux-firmware' 'kmod' 'mkinitcpio>=27')
  optdepends=('wireless-regdb: to set the correct wireless channels of your country')
  provides=("linux=${pkgver}")
  backup=("etc/mkinitcpio.d/${pkgbase}.preset")

  cd "linux-${_basekernel}"

  # get kernel version
  _kernver="$(make LOCALVERSION= kernelrelease)"

  mkdir -p "${pkgdir}"/{boot,usr/lib/modules}
  make LOCALVERSION= INSTALL_MOD_PATH="${pkgdir}/usr" INSTALL_MOD_STRIP=1 modules_install

  # systemd expects to find the kernel here to allow hibernation
  # https://github.com/systemd/systemd/commit/edda44605f06a41fb86b7ab8128dcf99161d2344
  cp arch/x86/boot/bzImage "${pkgdir}/usr/lib/modules/${_kernver}/vmlinuz"

  # Used by mkinitcpio to name the kernel
  echo "${pkgbase}" | install -Dm644 /dev/stdin "${pkgdir}/usr/lib/modules/${_kernver}/pkgbase"
  echo "${_basekernel}-${CARCH}" | install -Dm644 /dev/stdin "${pkgdir}/usr/lib/modules/${_kernver}/kernelbase"

  # add kernel version
  echo "${pkgver}-${pkgrel}-MANJARO x64" > "${pkgdir}/boot/${pkgbase}-${CARCH}.kver"

  # make room for external modules
  local _extramodules="extramodules-${_basekernel}${_kernelname:--MANJARO}"
  ln -s "../${_extramodules}" "${pkgdir}/usr/lib/modules/${_kernver}/extramodules"

  # add real version for building modules and running depmod from hook
  echo "${_kernver}" |
    install -Dm644 /dev/stdin "${pkgdir}/usr/lib/modules/${_extramodules}/version"

  # remove build and source links
  rm "${pkgdir}"/usr/lib/modules/${_kernver}/{source,build}

  # now we call depmod...
  depmod -b "${pkgdir}/usr" -F System.map "${_kernver}"
}

package_linux414-headers() {
  pkgdesc="Header files and scripts for building modules for ${pkgbase/linux/Linux} kernel"
  depends=('gawk' 'python' 'libelf' 'pahole')
  provides=("linux-headers=$pkgver")

  cd "linux-${_basekernel}"
  local _builddir="${pkgdir}/usr/lib/modules/${_kernver}/build"

  install -Dt "${_builddir}" -m644 Makefile .config Module.symvers
  install -Dt "${_builddir}/kernel" -m644 kernel/Makefile
  install -Dt "${_builddir}" -m644 vmlinux  

  mkdir "${_builddir}/.tmp_versions"

  cp -t "${_builddir}" -a include scripts

  install -Dt "${_builddir}/arch/x86" -m644 "arch/x86/Makefile"
  install -Dt "${_builddir}/arch/x86/kernel" -m644 "arch/x86/kernel/asm-offsets.s"

  cp -t "${_builddir}/arch/x86" -a "arch/x86/include"

  install -Dt "${_builddir}/drivers/md" -m644 drivers/md/*.h
  install -Dt "${_builddir}/net/mac80211" -m644 net/mac80211/*.h

  # https://bugs.archlinux.org/task/9912
  install -Dt "${_builddir}/drivers/media/dvb-core" -m644 drivers/media/dvb-core/*.h

  # https://bugs.archlinux.org/task/13146
  install -Dt "${_builddir}/drivers/media/i2c" -m644 drivers/media/i2c/msp3400-driver.h

  # https://bugs.archlinux.org/task/20402
  install -Dt "${_builddir}/drivers/media/usb/dvb-usb" -m644 drivers/media/usb/dvb-usb/*.h
  install -Dt "${_builddir}/drivers/media/dvb-frontends" -m644 drivers/media/dvb-frontends/*.h
  install -Dt "${_builddir}/drivers/media/tuners" -m644 drivers/media/tuners/*.h

  # https://bugs.archlinux.org/task/71392
  install -Dt "${_builddir}/drivers/iio/common/hid-sensors" -m644 drivers/iio/common/hid-sensors/*.h

  # add xfs and shmem for aufs building
  mkdir -p "${_builddir}"/{fs/xfs,mm}
  mkdir -p "${_builddir}"/fs/xfs/libxfs
  cp fs/xfs/libxfs/xfs_sb.h "${_builddir}"/fs/xfs/libxfs/xfs_sb.h

  #aufs4-util need
  sed -i "s:__user::g" "${_builddir}"/include/uapi/linux/aufs_type.h

  # copy in Kconfig files
  find . -name Kconfig\* -exec install -Dm644 {} "${_builddir}/{}" \;

  # add objtool for external module building and enabled VALIDATION_STACK option
  install -Dt "${_builddir}/tools/objtool" tools/objtool/objtool

  # remove unneeded architectures
  local _arch
  for _arch in "${_builddir}"/arch/*/; do
    [[ ${_arch} == */x86/ ]] && continue
    rm -r "${_arch}"
  done

  # remove documentation files
  rm -r "${_builddir}/Documentation"

  # strip scripts directory
  local file
  while read -rd '' file; do
    case "$(file -bi "$file")" in
      application/x-sharedlib\;*)      # Libraries (.so)
        strip $STRIP_SHARED "$file" ;;
      application/x-archive\;*)        # Libraries (.a)
        strip $STRIP_STATIC "$file" ;;
      application/x-executable\;*)     # Binaries
        strip $STRIP_BINARIES "$file" ;;
      application/x-pie-executable\;*) # Relocatable binaries
        strip $STRIP_SHARED "$file" ;;
    esac
  done < <(find "${_builddir}" -type f -perm -u+x ! -name vmlinux -print0 2>/dev/null)
  strip $STRIP_STATIC "${_builddir}/vmlinux"
  
  # remove unwanted files
  find ${_builddir} -name '*.orig' -delete
}
